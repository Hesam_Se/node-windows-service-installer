const Service = require('node-windows').Service;
const serviceInfo = require('../serviceInfo.json');


const svc = new Service({
    name: serviceInfo.name,
    description: serviceInfo.description,
    script: serviceInfo.path
});


svc.on('install',() => {
    console.info('service installed');
    svc.start();
});

svc.on('start', () => {
    console.info('service started');
});

svc.install();
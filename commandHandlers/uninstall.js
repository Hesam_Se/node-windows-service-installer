const Service = require('node-windows').Service;
const serviceInfo = require('../serviceInfo.json');


const svc = new Service({
    name: serviceInfo.name,
    description: serviceInfo.description,
    script: serviceInfo.path
});

svc.on('uninstall',function(){
    console.log('service uninstalled');
});

svc.on('error',function(err){
    console.log('service uninstalled');
    console.log(err);
});

svc.uninstall();
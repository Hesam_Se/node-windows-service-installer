const Service = require('node-windows').Service;
const serviceInfo = require('../serviceInfo.json');


const svc = new Service({
    name: serviceInfo.name,
    description: serviceInfo.description,
    script: serviceInfo.path
});

svc.on('stop',function(){
    console.log('service stopped');
});

svc.stop();